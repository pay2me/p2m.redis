# Starting Redis on a developer machine

1. Install latest [Docker](https://www.docker.com/products/docker)
1. If you want to save a persistent data and configuration between image starts, create a folder on a local disk for containing the data.
1. Share the disk in Docker Settings.
1. Run the command:
```cmd
docker run -p 6379:6379 -v <PATH-TO-DATA>:/data  redis:latest
```

# Instalation Docker and Redis on Ubuntu 16.04 LTE (xenial)

Just execute this command:
```bash
url=https://bitbucket.org/pay2me/p2m.redis/raw/master/ubuntu-xenial && curl -sS $url/install.sh | sudo bash /dev/stdin $url
```

This script installs or updates Docker, and installs or updates Redis to latest redis image.

All persistent data and configuration are stored in */data/redis* folder.

# Administration the Redis image on server

The RabbitMQ image runs as SystemD service on *redis-server* name. 

## Stop RabbitMQ
```bash
sudo systemctl stop redis-server
```

## Start RabbitMQ
```bash
sudo systemctl start redis-server
```