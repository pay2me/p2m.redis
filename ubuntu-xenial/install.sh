#!/bin/bash
set -eu

# Install Docker to Ubuntu 16.04 LTE

baseUrl=$1

#Check Docker already present in the system, if not -install it.
if ( ! which docker | grep -q '') || (whiptail --yesno --defaultno "Docker already installed. Update?" 20 60);
then 
    curl -sS $baseUrl/docker-install.sh | bash /dev/stdin
fi

#Check Redis service end update it
temp=$(mktemp)
curl -sS -o temp $baseUrl/data/redis-server.service

if (systemctl is-active redis-server.service); then
    systemctl stop redis-server.service
fi

#Create a folder for storing Redis persistent data 
if [ ! -f /data/redis ]; then
    mkdir -p /data/redis
fi
chmod -R a+rw /data/redis

#Pull latest image
docker pull redis:latest

mv temp /etc/systemd/system/redis-server.service

systemctl daemon-reload
systemctl start redis-server.service
systemctl --no-pager status redis-server.service
systemctl enable redis-server.service

echo "Congratulations! Redis is installed."