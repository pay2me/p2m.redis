 #!/bin/bash

##Install Docker to Ubuntu 16.04

##This script is automation of the instruction on official Docker site
##https://docs.docker.com/engine/installation/linux/ubuntu/

apt-get update

apt-get install --assume-yes apt-transport-https ca-certificates 

apt-key adv \
               --keyserver hkp://ha.pool.sks-keyservers.net:80 \
               --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

apt-get update

apt-get install --assume-yes linux-image-extra-$(uname -r) linux-image-extra-virtual
apt-get install --assume-yes docker-engine

systemctl enable docker
